//
//  Header.h
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef Header_h
#define Header_h

template<class T>
class LazyNonThreadSafeSingleton {
    
public:
    
    static T& instance() {
        static T singletonInstance;
        return singletonInstance;
    }
    
    LazyNonThreadSafeSingleton() {}
    virtual ~LazyNonThreadSafeSingleton() {}
    
    LazyNonThreadSafeSingleton(LazyNonThreadSafeSingleton const&) = delete;
    LazyNonThreadSafeSingleton& operator=(LazyNonThreadSafeSingleton const&) = delete;
    
};

#endif /* Header_h */
