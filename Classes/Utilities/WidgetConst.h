//
//  WidgetConst.h
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef WidgetConst_h
#define WidgetConst_h

extern const uint TEMP_OBJECT_LAYER;
extern const uint DYNAMIC_LAYER;

extern const std::string COINS_CHANGE_EVENT;
extern const std::string LIVES_CHANGE_EVENT;

extern const std::string FIELD_CHANGE_EVENT;

extern const cocos2d::Vec2 TENT_POSITION;

#endif /* WidgetConst_h */
