//
//  WidgetConst.c
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "WidgetConst.h"

const uint TEMP_OBJECT_LAYER = 999;
const uint DYNAMIC_LAYER = 1000;

const std::string COINS_CHANGE_EVENT = "game_coins_change";
const std::string LIVES_CHANGE_EVENT = "game_lives_change";

const std::string FIELD_CHANGE_EVENT = "field_change";

extern const cocos2d::Vec2 TENT_POSITION = cocos2d::Vec2(1,9);
