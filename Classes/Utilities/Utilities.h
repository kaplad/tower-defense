//
//  Header.h
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#ifndef Utilities_h
#define Utilities_h

#define make_baseclass(class) typedef class baseclass;

#define make_create(__TYPE__)                               \
template <typename ...Args>                                 \
static __TYPE__* create(const Args& ...args) {              \
    auto instance = new (std::nothrow) __TYPE__(args...);   \
    if (not (instance && instance->init())) {               \
        CC_SAFE_DELETE(instance);                           \
        return nullptr;                                     \
    }                                                       \
                                                            \
    instance->autorelease();                                \
    return instance;                                        \
}

#endif /* Header_h */
