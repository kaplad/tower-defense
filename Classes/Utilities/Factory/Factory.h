//
//  Factory.h
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef Factory_h
#define Factory_h

#include "json/document.h"

template <class Key, class Value>
class ClassFactory {
    
public:
    void init(const rapidjson::Document& document) {
        enumerateCLasses();
        load(document);
    }
    
    //
    
    const Value* objectInfo(const Key& name) const {
        auto foundObjectInfo = _classInstances.find(name);
        if(foundObjectInfo == _classInstances.end())
            throw std::exception();
        
        return foundObjectInfo->second;
    }
    
    //
    
    virtual ~ClassFactory() {}
    
protected:
    //
    virtual void load(const rapidjson::Document& document) = 0;
    virtual void enumerateCLasses() = 0;
    
    Value* create(const Key& type) const {
        auto foundClass = _classes.find(type);
        if(foundClass == _classes.end())
            throw std::exception();
        
        return foundClass->second();
    }
    
protected:
    std::unordered_map<Key, std::function<Value* ()> > _classes;
    std::unordered_map<Key, cocos2d::RefPtr<Value> > _classInstances;
};

#endif /* Factory_h */
