//
//  MultiTool.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#include "MultiTool.hpp"
#include "Command.hpp"
#include "WidgetConst.h"

void MultiTool::init(const gamestate::CommandProcessorPtr& processor,
                     GameFieldWidget* const parent,
                     GameObjectWidget* const childtoCreate,
                     TileValueStrategy* const strategy) {
    
    _processor = processor;
    _gamefield = parent;
    _newObject = childtoCreate;
    _strategy = strategy;
    _state = ToolState::UNACTIVE;
}

//
//---
//

void MultiTool::deinit() {
    deactivate();
    
    _processor = nullptr;
    _gamefield = nullptr;
    _newObject = nullptr;
    _strategy = nullptr;
    _state = ToolState::UNACTIVE;
}

//
//---
//

bool MultiTool::isActive() const {
    return _state == ToolState::ACTIVE;
}

//
//---
//

MultiTool::~MultiTool() {
}

//
//------------------------------------------------------------------------------
//

void CreateObjectTool::activate() {
    if (isActive())
        return;
    
    _gamefield->addTempObject(_newObject);
    _state = ToolState::ACTIVE;
}

//
//---
//

void CreateObjectTool::deactivate() {
    _gamefield->removeTempObject(_newObject);
    _state = ToolState::UNACTIVE;
}

//
//---
//

void CreateObjectTool::update(const cocos2d::Point& position) {
	if (not _gamefield)
		return;
	
    const auto convertedlocation = _gamefield->convertToNodeSpace(position);
    const bool onfield = _gamefield->getBoundingBox().containsPoint(position);
    
    if (onfield) {
        if (isActive() and _strategy->tileValue(_gamefield->nodeToGrid(convertedlocation)) > 0)
            _newObject->setPosition(_gamefield->roundNodeCoord(convertedlocation));
        else
            activate();
    }
    else
        deactivate();
}

//
//---
//

void CreateObjectTool::makeAction() {
    if (not isActive())
        return;
    
    deactivate();
    
    auto addtowerCommand = new gamestate::AddTowerCommand(_gamefield->nodeToGrid(_newObject->getPosition()),
                                                          _newObject->getInfo()->getName());
    auto _newObject_strong = _newObject;
    auto _gamefield_strong = _gamefield;
    addtowerCommand->setSuccessCallback([_newObject_strong, _gamefield_strong]() {
        const auto tag = _gamefield_strong->generateTagByGridPosition(_gamefield_strong->nodeToGrid(_newObject_strong->getPosition()));
        _newObject_strong->setTag(tag);
        _gamefield_strong->addObject(_newObject_strong);
    });
    
    addtowerCommand->setErrorCallback([](const gamestate::CommandResult& message) {
        cocos2d::MessageBox(message.c_str(), "Info");
    });
    
    _processor->executeCommand(addtowerCommand);
}
