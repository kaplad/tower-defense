//
//  MultiTool.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef MultiTool_hpp
#define MultiTool_hpp

#include "TileValueStrategy.hpp"
#include "CommandProcessor.hpp"
#include "GameObjectWidget.hpp"
#include "GameFieldWidget.hpp"

enum class ToolState {
    UNKNOWN,
    ACTIVE,
    UNACTIVE
};

class MultiTool: public cocos2d::Ref {
    make_baseclass(cocos2d::Ref)
    
public:
    virtual void init(const gamestate::CommandProcessorPtr& processor,
                      GameFieldWidget* const parent,
                      GameObjectWidget* const childtoCreate,
                      TileValueStrategy* const strategy);
    
    virtual void deinit();
    
    bool isActive() const;
    
    //
    
    virtual void update(const cocos2d::Point&) = 0;
    
    virtual void activate() = 0;
    virtual void deactivate() = 0;
    
    virtual void makeAction() = 0;
    
    virtual ~MultiTool();
    
protected:

    
protected:
    gamestate::CommandProcessorPtr _processor;
    
    cocos2d::RefPtr<TileValueStrategy> _strategy;
    cocos2d::RefPtr<GameFieldWidget> _gamefield;
    cocos2d::RefPtr<GameObjectWidget> _newObject;
    
    ToolState _state;
};

//
//---
//

class CreateObjectTool: public MultiTool {
    make_baseclass(MultiTool)
    
public:
    virtual void activate() override;
    virtual void deactivate() override;
    
    virtual void update(const cocos2d::Point&) override;
    virtual void makeAction() override;
};

#endif /* MultiTool_hpp */
