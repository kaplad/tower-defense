//
//  TileValueStrategy.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef TileValueStrategy_hpp
#define TileValueStrategy_hpp

#include "GameState.hpp"

class GameFieldWidget;

class TileValueStrategy: public cocos2d::Ref {
    make_baseclass(cocos2d::Ref)
    
public:
    TileValueStrategy(GameFieldWidget* const,
                      const gamestate::FieldInfo* field);
    
    virtual uint tileValue(const cocos2d::Vec2&) const;
    
    virtual ~TileValueStrategy() {}
protected:
    bool cellIsOccupied(const cocos2d::Vec2& position,
                        cocos2d::Node* const layer) const;
    
    bool cellIsOccupied(const cocos2d::Vec2& position,
                        cocos2d::TMXLayer* const layer) const;
    
    bool cellIsOccupied(const cocos2d::Vec2& position,
                        const gamestate::FieldInfo* const field,
                        GameFieldWidget* const) const;
    
    
    virtual bool skip(cocos2d::Node* const layer) const = 0;
    
protected:
    const gamestate::FieldInfo* _field;
    GameFieldWidget* _gameField;
};

//
//------------------------------------------------------------------------------
//

class TowerTileValueSrategy: public TileValueStrategy {
    make_baseclass(TileValueStrategy)
    
public:
    TowerTileValueSrategy(GameFieldWidget* const map,
                          const gamestate::FieldInfo* field): baseclass(map, field) {}
protected:
    virtual bool skip(cocos2d::Node* const layer) const override;
};

//
//------------------------------------------------------------------------------
//

class EnemyTileValueSrategy: public TileValueStrategy {
    make_baseclass(TileValueStrategy)
    
public:
    EnemyTileValueSrategy(GameFieldWidget* const map,
                          const gamestate::FieldInfo* field): baseclass(map, field) {}

protected:
    virtual bool skip(cocos2d::Node* const layer) const override;
};

#endif /* TileValueStrategy_hpp */
