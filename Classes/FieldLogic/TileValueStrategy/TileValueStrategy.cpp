//
//  TileValueStrategy.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#include "TileValueStrategy.hpp"
#include "GameFieldWidget.hpp"

TileValueStrategy::TileValueStrategy(GameFieldWidget* const gamefield,
                                     const gamestate::FieldInfo* field):
_field(field),
_gameField(gamefield)
{
}

//
//---
//

uint TileValueStrategy::tileValue(const cocos2d::Vec2& positionToCheck) const {
    
    for (const auto layer : _gameField->getChildren()) {
        if (skip(layer))
            continue;
        
        if (cellIsOccupied(positionToCheck, layer))
            return 0;
    }
    
    if (cellIsOccupied(positionToCheck, _field, _gameField))
        return 0;
    
    return 1;
}

//
//---
//

bool TileValueStrategy::cellIsOccupied(const cocos2d::Vec2& position,
                                       cocos2d::Node* const layer) const {
    const auto tmx = dynamic_cast<cocos2d::TMXLayer*>(layer);
    if (tmx)
        return cellIsOccupied(position, tmx);
    
    return false;
}

//
//---
//

bool TileValueStrategy::cellIsOccupied(const cocos2d::Vec2& position,
                                       cocos2d::TMXLayer* const layer) const {
    
    return layer->getTileGIDAt(cocos2d::Vec2(position.x,
                                             std::abs(position.y - 15))) > 0; //workaround
}

//
//---
//

bool TileValueStrategy::cellIsOccupied(const cocos2d::Vec2& position,
                                       const gamestate::FieldInfo* const field,
                                       GameFieldWidget* const map) const {
    const auto objectbycoords = field->getObject(position);
    if (objectbycoords)
        return true;
    
    return false;
}

//
//------------------------------------------------------------------------------
//

bool TowerTileValueSrategy::skip(cocos2d::Node* const layer) const {
    const auto tmx = dynamic_cast<cocos2d::TMXLayer*>(layer);
    if (tmx)
        return tmx->getLayerName() == "Ground";
    
    return true;
}

//
//------------------------------------------------------------------------------
//

bool EnemyTileValueSrategy::skip(cocos2d::Node* const layer) const {
    const auto tmx = dynamic_cast<cocos2d::TMXLayer*>(layer);
    if (tmx)
        return tmx->getLayerName() == "Ground" or
               tmx->getLayerName() == "Tent" or
               tmx->getLayerName() == "Road";
    
    return true;
}
