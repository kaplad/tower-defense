//
//  GameController.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "GameController.hpp"
#include "ObjectInfoProvider.hpp"
#include "GameObjectWidget.hpp"
#include "TileValueStrategy.hpp"
#include "GameFieldWidget.hpp"
#include "WidgetConst.h"
#include "GameClock.h"
#include "PathFinder.hpp"
#include "MainMenu.hpp"

GameController::GameController(const gamestate::CommandProcessorPtr& processor,
                               GameFieldWidget* const gamefield):
_processor(processor),
_gamefield(gamefield),
_pathFinder()
{
    _fieldcontrollers = {new TowerController(_processor, _gamefield),
						 new EnemyController(_processor, _gamefield)};

	auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
	
	dispatcher->addCustomEventListener(FIELD_CHANGE_EVENT, [this](cocos2d::EventCustom* event){
		auto enemyontent = _processor->getState()->fieldInfo()->getObject(TENT_POSITION);
		if (enemyontent) {
			cocos2d::MessageBox("You lost", "Info");
			cocos2d::Director::getInstance()->replaceScene(MainMenu::createScene());
		}
	});
}

//
//---
//

void GameController::update() const {
    updateWave();
    updateObjects();
}

//
//---
//

void GameController::updateWave() const {
    const auto now = GameClock::instance().nowtime();
    const time_t lastspawn = _processor->getState()->waveInfo()->getLastSpawnTime();
    const uint spawnPause = _processor->getState()->waveInfo()->getRespawnTime();
    
    if (now < lastspawn + spawnPause)
        return;
    
    if (_processor->getState()->waveInfo()->getEnemyList().empty())
        return;
    
    const auto newObject = _processor->getState()->waveInfo()->nextEnemy();
    const auto position = findEnemyPosition();
    const auto _gamefield_strong = _gamefield;
	
	auto foundPath = _pathFinder.findPath(_pathFinder.generateMapWithStrategy(EnemyTileValueSrategy(_gamefield,
																									_processor->getState()->fieldInfo()),
																			  _gamefield->getMapSize()),
										  position,
										  TENT_POSITION);
	
    auto createcommand = new gamestate::AddEnemyCommand(position, newObject);
    createcommand->setSuccessCallback([position, newObject, _gamefield_strong, foundPath]() {
        const auto objectinfo = dynamic_cast<const EnemyObjectInfoBase*>(ObjectInfoProvider::instance().objectInfo(newObject));
        auto object = dynamic_cast<EnemyObjectWidget*>(objectinfo->prototype());
		
        object->setTag(_gamefield_strong->generateTagByGridPosition(position));
        object->setPosition(_gamefield_strong->gridToNode(position));
        object->setHealth(objectinfo->getHealth());
		object->setPath(foundPath);
		
        _gamefield_strong->addObject(object);
    });
    
    _processor->executeCommand(createcommand);

}

//
//---
//

void GameController::updateObjects() const {
    for (const auto& controller : _fieldcontrollers) {
        auto layer = _gamefield->getChildByTag(DYNAMIC_LAYER);

        for (auto child : layer->getChildren()) {
            auto object = static_cast<GameObjectWidget*>(child);
            object->accept(controller);
        }
    }
}

//
//---
//

cocos2d::Vec2 GameController::findEnemyPosition() const {
    std::vector<cocos2d::Vec2> result;
    
    cocos2d::RefPtr<TileValueStrategy> strategy = new EnemyTileValueSrategy(_gamefield,
                                                                            _processor->getState()->fieldInfo());
    
    for (int i = 0; i < _gamefield->getMapSize().height; ++i) {
        const cocos2d::Vec2 pointtocheck (_gamefield->getMapSize().width - 1, i);
        
        if (strategy->tileValue(pointtocheck) > 0)
            result.push_back(pointtocheck);
    }
    
    if (result.empty()) return cocos2d::Vec2(); // todo
    
    return result[rand() & result.size()];
}


//
//---
//

GameController::~GameController() {
	auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
	dispatcher->removeCustomEventListeners(COINS_CHANGE_EVENT);
	dispatcher->removeCustomEventListeners(FIELD_CHANGE_EVENT);
}
