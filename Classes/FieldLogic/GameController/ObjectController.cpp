//
//  ObjectController.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "ObjectController.hpp"
#include "GameFieldWidget.hpp"
#include "GameObjectWidget.hpp"
#include "GameClock.h"

ObjectVisitor::ObjectVisitor(const gamestate::CommandProcessorPtr& processor,
                             GameFieldWidget* const gamefield):
_processor(processor),
_gamefield(gamefield)
{
}

//
//------------------------------------------------------------------------------
//

void TowerController::visit(TowerObjectWidget* object) const {
    const auto towerposition = _gamefield->nodeToGrid(object->getPosition());
    auto stateobject = _processor->getState()->fieldInfo()->getObject(towerposition);
    if (!stateobject)
        return;
    
    if (GameClock::instance().nowtime() < stateobject->getNextUpdateTime())
        return;
    
    auto towerinfo = static_cast<const TowerObjectInfoBase*>(object->getInfo());
    auto enemy = findNearestEnemy(towerposition, towerinfo->getHitRadius());
    if (!enemy)
        return;
    
    const auto moveObject = new gamestate::TowerAttack(towerposition,
                                                       _gamefield->nodeToGrid(enemy->getPosition()));
    
    moveObject->setSuccessCallback([object, enemy, towerinfo]() {
        enemy->setHealth(enemy->getHealth() - towerinfo->getHitPower());
        object->attack(enemy);
    });
    
    _processor->executeCommand(moveObject);

}

//
//---
//

EnemyObjectWidget* TowerController::findNearestEnemy(const cocos2d::Vec2& gridPosition, uint radius) const {

    // omg algorithm :)
    std::max(int(gridPosition.x - radius), 0);
    
    for (int xposition = std::max(int(gridPosition.x - radius), 0);
            xposition <= std::min(gridPosition.x + radius, _gamefield->getMapSize().width);
            ++xposition)
    {
        for (int yposition = std::max(int(gridPosition.y - radius), 0);
                yposition <= std::min(gridPosition.y + radius, _gamefield->getMapSize().height);
                ++yposition)
        {
            const auto checkposition = cocos2d::Vec2(xposition, yposition);
            if (checkposition == gridPosition)
                continue;
            
            auto foundobject = _gamefield->getObject<EnemyObjectWidget>(checkposition);
            if (foundobject && foundobject->getHealth() > 0)
                return foundobject;
        }
    }
    
    return nullptr;
}

//
//------------------------------------------------------------------------------
//

void EnemyController::visit(EnemyObjectWidget* object) const {
    const auto gridposition = _gamefield->nodeToGrid(object->getPosition());
    auto stateobject = _processor->getState()->fieldInfo()->getObject(gridposition);
    if (!stateobject)
        return;
    
    if (GameClock::instance().nowtime() < stateobject->getNextUpdateTime())
        return;
    
    auto _gamefield_strong = _gamefield;
    const auto moveObject = new gamestate::MoveEnemyCommand(gridposition, object->nextPathElement());
    moveObject->setSuccessCallback([object, _gamefield_strong, gridposition]() {
        auto action = cocos2d::MoveTo::create(0.1,
                                              _gamefield_strong->gridToNode(object->nextPathElement()));
        object->runAction(action);
		object->removePassedElement();
    });
    
    _processor->executeCommand(moveObject);
}

