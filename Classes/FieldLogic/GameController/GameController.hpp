//
//  GameController.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef GameController_hpp
#define GameController_hpp

#include "CommandProcessor.hpp"
#include "ObjectController.hpp"
#include "PathFinder.hpp"

class GameFieldWidget;

class GameController: public cocos2d::Ref {
public:
    GameController(const gamestate::CommandProcessorPtr& processor,
                   GameFieldWidget* const gamefield);
	~GameController();
	
    void update() const;
    
protected:
    void updateWave() const;
    void updateObjects() const;
    
    //
    
    cocos2d::Vec2 findEnemyPosition() const;
    
protected:
    const gamestate::CommandProcessorPtr _processor;
    GameFieldWidget* const _gamefield;
    ObjectControllerList _fieldcontrollers;
	
	const PathFinder _pathFinder;
};

typedef cocos2d::RefPtr<GameController> GameControllerPtr;

#endif /* GameController_hpp */
