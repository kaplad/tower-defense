//
//  ObjectController.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef ObjectController_hpp
#define ObjectController_hpp

#include "CommandProcessor.hpp"

class GameFieldWidget;
class TowerObjectWidget;
class EnemyObjectWidget;

class ObjectVisitor: public cocos2d::Ref {
    
public:
    ObjectVisitor(const gamestate::CommandProcessorPtr& processor,
                  GameFieldWidget* const gamefield);
    virtual ~ObjectVisitor() {}
    
    virtual void visit(TowerObjectWidget* object) const {}
    virtual void visit(EnemyObjectWidget* object) const {}
    
protected:
    const gamestate::CommandProcessorPtr _processor;
    GameFieldWidget* const _gamefield;
};

typedef cocos2d::RefPtr<ObjectVisitor> ObjectVisitorPtr;
typedef std::list<ObjectVisitorPtr> ObjectControllerList;

//
//------------------------------------------------------------------------------
//

class TowerController: public ObjectVisitor {
    make_baseclass(ObjectVisitor)
    
public:
    using ObjectVisitor::ObjectVisitor;
    virtual void visit(TowerObjectWidget* object) const;
    
    EnemyObjectWidget* findNearestEnemy(const cocos2d::Vec2& gridPosition, uint radius) const;
    
};

//
//------------------------------------------------------------------------------
//

class EnemyController: public ObjectVisitor {
    make_baseclass(ObjectVisitor)
    
public:
    using ObjectVisitor::ObjectVisitor;
    virtual void visit(EnemyObjectWidget* object) const;
};

#endif /* ObjectController_hpp */
