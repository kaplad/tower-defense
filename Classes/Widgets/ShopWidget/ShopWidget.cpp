//
//  ShopWidget.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#include "ShopWidget.hpp"
#include "ShopItemWidget.hpp"
#include "MultiTool.hpp"
#include "TileValueStrategy.hpp"

ShopWidget::ShopWidget(const gamestate::CommandProcessorPtr& processor,
                       GameFieldWidget* const gamefield):
_gamefield(gamefield),
_processor(processor){
    
}

//
//---
//

bool ShopWidget::init() {
    if (!baseclass::init())
        return false;
    
    auto _touchlistener = cocos2d::EventListenerTouchOneByOne::create();
    cocos2d::RefPtr<MultiTool> tool = new CreateObjectTool();
    
    _touchlistener->onTouchBegan = [this, tool](cocos2d::Touch* touch, cocos2d::Event*) {

		const auto shopitem = findShopItem(touch->getLocation());
		if (not shopitem)
			return true;
		
        tool->init(_processor,
                   _gamefield,
                   shopitem->prototype(),
                   new TowerTileValueSrategy(_gamefield,
                                             _processor->getState()->fieldInfo()));
        
        return true;
    };
    
    _touchlistener->onTouchMoved = [this, tool](cocos2d::Touch* touch, cocos2d::Event*) {
		tool->update(touch->getLocation());
        
        return true;
    };
    
    _touchlistener->onTouchEnded = [this, tool](cocos2d::Touch* touch, cocos2d::Event*) {
        tool->makeAction();
        return true;
    };
    
    getEventDispatcher()->addEventListenerWithFixedPriority(_touchlistener, 1000);
    
    return true;
}

ShopWidget::~ShopWidget() {
    getEventDispatcher()->removeEventListener(_touchlistener);
    _touchlistener = nullptr;
}

//
//---
//

void ShopWidget::addChild(ShopItemWidget * child) {
    if (!child)
        return;
    
    const auto newChildPosition = cocos2d::Vec2(0, getChildrenContentSize().height);
    child->setPosition(newChildPosition);
    child->setAnchorPoint(cocos2d::Vec2());
    
    baseclass::addChild(child);
}

//
//---
//

cocos2d::Size ShopWidget::getChildrenContentSize() const {
    cocos2d::Size result;
    
    for( const auto child : getChildren()) {
        result.height += child->getContentSize().height;
    }
    
    return result;
}

//
//---
//

ShopItemWidget* ShopWidget::findShopItem(const cocos2d::Vec2& position) {
	for (auto child : getChildren()) {
		const auto convertedlocation = child->convertToNodeSpace(position);
		const bool onchild = child->getBoundingBox().containsPoint(convertedlocation);
		if (onchild)
			return dynamic_cast<ShopItemWidget*>(child);
	}
	
	return nullptr;
}
