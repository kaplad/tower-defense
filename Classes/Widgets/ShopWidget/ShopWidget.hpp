//
//  ShopWidget.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#ifndef ShopWidget_hpp
#define ShopWidget_hpp

#include "CommandProcessor.hpp"
#include "GameFieldWidget.hpp"

class ShopItemWidget;

class ShopWidget: public cocos2d::Node {
    make_baseclass(cocos2d::Node)
    
public:
    virtual void addChild(ShopItemWidget * child);
    make_create(ShopWidget)
    
    ~ShopWidget();
    
protected:
    ShopWidget(const gamestate::CommandProcessorPtr& processor,
               GameFieldWidget* const gamefield);
    bool init() override;
    
    cocos2d::Size getChildrenContentSize() const;
	
	ShopItemWidget* findShopItem(const cocos2d::Vec2& position);
	
protected:
    cocos2d::RefPtr<GameFieldWidget> _gamefield;
    gamestate::CommandProcessorPtr _processor;
    
    cocos2d::RefPtr<cocos2d::EventListenerTouchOneByOne> _touchlistener;
};

#endif /* ShopWidget_hpp */
