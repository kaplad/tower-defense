//
//  ShopItemWidget.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#ifndef ShopItemWidget_hpp
#define ShopItemWidget_hpp

#include "ObjectInfoProvider.hpp"

class GameObjectWidget;

class ShopItemWidget: public cocos2d::Node {
    make_baseclass(cocos2d::Node)
public:
    make_create(ShopItemWidget)
    GameObjectWidget* prototype();
    
protected:
    bool init();
    ShopItemWidget(const std::string& item);
    
protected:
    const TowerObjectInfoBase* _shopItem;
};

#endif /* ShopItemWidget_hpp */
