//
//  ShopItemWidget.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#include "ShopItemWidget.hpp"


ShopItemWidget::ShopItemWidget(const std::string& item) {
    _shopItem = dynamic_cast<const TowerObjectInfoBase*>(ObjectInfoProvider::instance().objectInfo(item));
}

//
//---
//

bool ShopItemWidget::init() {
    if(!baseclass::init())
        return false;
	
    auto sprite = cocos2d::Sprite::create(_shopItem->getImage());
    sprite->setAnchorPoint(cocos2d::Vec2());
    setContentSize(sprite->getContentSize());
    addChild(sprite);
	
	{
		auto power = cocos2d::Label::createWithTTF("Power: " + std::to_string(_shopItem->getHitPower()),
													 "fonts/Marker Felt.ttf", 26);
		power->setPosition(120, 110);
		power->setAnchorPoint(cocos2d::Vec2::ANCHOR_BOTTOM_LEFT);
		addChild(power);
	}
	{
		auto radius = cocos2d::Label::createWithTTF("Radius: " + std::to_string(_shopItem->getHitRadius()),
												   "fonts/Marker Felt.ttf", 26);
		radius->setPosition(120, 80);
		radius->setAnchorPoint(cocos2d::Vec2::ANCHOR_BOTTOM_LEFT);
		addChild(radius);
	}
	
    return true;
}

//
//---
//

GameObjectWidget* ShopItemWidget::prototype() {
    return _shopItem->prototype();
}
