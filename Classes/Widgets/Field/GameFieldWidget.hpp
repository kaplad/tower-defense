//
//  GameFieldWidget.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef GameFieldWidget_hpp
#define GameFieldWidget_hpp

#include "WidgetConst.h"

class GameObjectWidget;

class GameFieldWidget: public cocos2d::TMXTiledMap {
public:
    static GameFieldWidget* create(const std::string& tmxFile);
    
    cocos2d::Vec2 gridToNode(const cocos2d::Vec2& grid);
    cocos2d::Vec2 roundNodeCoord(const cocos2d::Vec2& grid);
    cocos2d::Vec2 nodeToGrid(const cocos2d::Vec2& node);
    
    uint generateTagByGridPosition(const cocos2d::Vec2&) const;
    
    void addObject(GameObjectWidget*);
    void removeObject(GameObjectWidget*);
    
    void addTempObject(GameObjectWidget*);
    void removeTempObject(GameObjectWidget*);
    
    template <class T>
    T* getObject(const cocos2d::Vec2& position) {
        auto dynamiclayer = getChildByTag(DYNAMIC_LAYER);
        if (dynamiclayer)
            return dynamic_cast<T*>(dynamiclayer->getChildByTag(generateTagByGridPosition(position)));
        
        return nullptr;
    }
};

#endif /* GameFieldWidget_hpp */
