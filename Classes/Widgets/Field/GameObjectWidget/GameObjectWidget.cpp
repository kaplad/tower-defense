//
//  GameObjectWidget.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#include "GameObjectWidget.hpp"
#include "ObjectInfo.hpp"
#include "ObjectController.hpp"

GameObjectWidget::GameObjectWidget(const ObjectInfo* info):
baseclass(),
_info(info),
_health(0)
{
}

//
//---
//

bool GameObjectWidget::init() {
    if (!baseclass::init())
        return false;

    setScale(0.5f);
    setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE);
    
    addChild(cocos2d::Sprite::create(_info->getImage()));
    
    return true;
}

//
//------------------------------------------------------------------------------
//

bool TowerObjectWidget::init() {
    if (!baseclass::init())
        return false;
    
    setScale(0.5f);
    setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE);
    
    addChild(cocos2d::Sprite::create(_info->getImage()));
    
    return true;
}

//
//---
//

void TowerObjectWidget::accept(ObjectVisitor* visitor) {
    visitor->visit(this);
}

//
//------------------------------------------------------------------------------
//

void TowerObjectWidget::attack(EnemyObjectWidget* const enemy) {
    auto arrow = cocos2d::Sprite::create("arrow.png");
    arrow->setScale(1.5);
    arrow->setPosition(getPosition());
    arrow->setRotation(CC_RADIANS_TO_DEGREES(cocos2d::Vec2::angle(arrow->getPosition(),
                                                                  enemy->getPosition())));
    getParent()->addChild(arrow);
    
    
    const bool needremove = enemy->getHealth() <= 0;
    
    auto showDamageCallback = cocos2d::CallFunc::create([enemy](){
        auto damage = cocos2d::Sequence::create(cocos2d::FadeTo::create(0.3, 125),
                                                cocos2d::FadeTo::create(0.3, 255),
                                                nullptr);
        enemy->setCascadeOpacityEnabled(true);
        enemy->runAction(damage);
    });
    
    auto removeEnemy = cocos2d::CallFunc::create(std::bind(&cocos2d::Node::removeFromParent, enemy));
    
    auto actions = cocos2d::Sequence::create(cocos2d::MoveTo::create(0.7, enemy->getPosition()),
                                             cocos2d::CallFunc::create(std::bind(&cocos2d::Node::removeFromParent, arrow)),
                                             needremove? removeEnemy : showDamageCallback,
                                             nullptr);
    
    arrow->runAction(actions);
}

//
//------------------------------------------------------------------------------
//

bool EnemyObjectWidget::init() {
    if (!baseclass::init())
        return false;
    
    setScale(0.65f);
    setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE);
    
    addChild(cocos2d::Sprite::create(_info->getImage()));
    
    return true;
}

//
//---
//

void EnemyObjectWidget::accept(ObjectVisitor* visitor) {
    visitor->visit(this);
}

//
//---
//

void EnemyObjectWidget::setPath(const std::list<cocos2d::Vec2>& path) {
    _path = path;
}

//
//---
//

cocos2d::Vec2 EnemyObjectWidget::nextPathElement() const {
    if (_path.empty())
        return cocos2d::Vec2();
    
    return _path.front();
}

//
//---
//


void EnemyObjectWidget::removePassedElement() {
    if (not _path.empty())
        _path.pop_front();
}

//
//---
//

bool EnemyObjectWidget::completedPath() const {
	return _path.empty();
}
