//
//  GameObjectWidget.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef GameObjectWidget_hpp
#define GameObjectWidget_hpp

#include "ObjectInfo.hpp"

class ObjectVisitor;
class EnemyObjectWidget;

class GameObjectWidget: public cocos2d::Node {
    make_baseclass(cocos2d::Node)
    
public:
    GameObjectWidget(const ObjectInfo*);
    
    CC_SYNTHESIZE_READONLY(const ObjectInfo*, _info, Info)
    
    virtual void accept(ObjectVisitor* visitor) = 0;

    CC_SYNTHESIZE_PASS_BY_REF(uint, _health, Health);
    
protected:
    bool init() override;
};

//
//------------------------------------------------------------------------------
//

class TowerObjectWidget: public GameObjectWidget {
    make_baseclass(GameObjectWidget)
    
public:
    make_create(TowerObjectWidget)
    TowerObjectWidget(const ObjectInfo* info): baseclass(info){}
    virtual void accept(ObjectVisitor* visitor) override;
    
    void attack(EnemyObjectWidget* const enemy);
    
protected:
    bool init() override;
};

//
//------------------------------------------------------------------------------
//

class EnemyObjectWidget: public GameObjectWidget {
    make_baseclass(GameObjectWidget)
    
public:
    make_create(EnemyObjectWidget)
    EnemyObjectWidget(const ObjectInfo* info): baseclass(info){}
    virtual void accept(ObjectVisitor* visitor) override;
    
	void setPath(const std::list<cocos2d::Vec2>& path);
    cocos2d::Vec2 nextPathElement() const;
    void removePassedElement();
	bool completedPath() const;
	
protected:
    bool init() override;
    
protected:
    std::list<cocos2d::Vec2> _path;
};

#endif /* GameObjectWidget_hpp */
