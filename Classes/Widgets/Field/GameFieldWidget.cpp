//
//  GameFieldWidget.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "GameFieldWidget.hpp"
#include "GameObjectWidget.hpp"

GameFieldWidget * GameFieldWidget::create(const std::string& tmxFile) {
    GameFieldWidget *ret = new (std::nothrow) GameFieldWidget();
    if (ret->initWithTMXFile(tmxFile))
    {
        ret->addChild(cocos2d::Layer::create(), DYNAMIC_LAYER, DYNAMIC_LAYER);
        ret->addChild(cocos2d::Layer::create(), TEMP_OBJECT_LAYER, TEMP_OBJECT_LAYER);
        ret->setScale(0.75f);
        
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

//
//---
//

cocos2d::Vec2 GameFieldWidget::roundNodeCoord(const cocos2d::Vec2& node) {
    const int relativePositionX = node.x / getTileSize().width;
    const int relativePositionY = node.y / getTileSize().height;
    
    return cocos2d::Vec2(relativePositionX * getTileSize().width + getTileSize().width / 2,
                         relativePositionY * getTileSize().height + getTileSize().height / 2);
}

//
//---
//

cocos2d::Vec2 GameFieldWidget::nodeToGrid(const cocos2d::Vec2& node) {
    return cocos2d::Vec2((int)node.x / (int)getTileSize().width,
                         (int)node.y / (int)getTileSize().height);
}

//
//---
//

cocos2d::Vec2 GameFieldWidget::gridToNode(const cocos2d::Vec2& grid) {
    return cocos2d::Vec2(grid.x * getTileSize().width + getTileSize().width / 2,
                         grid.y * getTileSize().height + getTileSize().height / 2);
}

//
//---
//

uint GameFieldWidget::generateTagByGridPosition(const cocos2d::Vec2& grid) const {
    return grid.y * getMapSize().height + grid.x;
}

//
//---
//

void GameFieldWidget::addObject(GameObjectWidget* object) {
    auto dynamiclayer = getChildByTag(DYNAMIC_LAYER);
    if (dynamiclayer)
        dynamiclayer->addChild(object);
}

//
//---
//

void GameFieldWidget::removeObject(GameObjectWidget* object) {
    auto dynamiclayer = getChildByTag(DYNAMIC_LAYER);
    if (dynamiclayer)
        dynamiclayer->removeChild(object);
}

//
//---
//

void GameFieldWidget::addTempObject(GameObjectWidget* object) {
    auto dynamiclayer = getChildByTag(TEMP_OBJECT_LAYER);
    if (dynamiclayer)
        dynamiclayer->addChild(object);
}

//
//---
//

void GameFieldWidget::removeTempObject(GameObjectWidget* object) {
    auto dynamiclayer = getChildByTag(TEMP_OBJECT_LAYER);
    if (dynamiclayer)
        dynamiclayer->removeChild(object);
}
