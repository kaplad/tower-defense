//
//  GameState.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef GameState_hpp
#define GameState_hpp

#include "json/document.h"

namespace gamestate {
    
    class GameStateSerializable: public cocos2d::Ref {
    public:
        virtual void load(const rapidjson::Value& state) = 0;
        virtual void save(rapidjson::Value& out) const { /*todo*/ }
        
        virtual ~GameStateSerializable(){}
    };
    
    //
    //--------------------------------------------------------------------------
    //
    
    class ActorInfo: public GameStateSerializable {
    public:
        void load(const rapidjson::Value& state) override;
        
        CC_SYNTHESIZE_PASS_BY_REF(uint, _coins, Coins);
        CC_SYNTHESIZE_PASS_BY_REF(uint, _lives, Lives);
    };
    
    typedef cocos2d::RefPtr<ActorInfo> ActorInfoPtr;
    
    //
    //--------------------------------------------------------------------------
    //
    
    class GameObject: public GameStateSerializable {
    public:
        void load(const rapidjson::Value& state) override;
        
        CC_SYNTHESIZE_PASS_BY_REF(std::string, _type, Type);
        CC_SYNTHESIZE_PASS_BY_REF(cocos2d::Vec2, _position, Position);
        CC_SYNTHESIZE_PASS_BY_REF(uint, _health, Health);
        CC_SYNTHESIZE_PASS_BY_REF(time_t, _nextUpdateTime, NextUpdateTime);
    };
    
    typedef cocos2d::RefPtr<GameObject> GameObjectObjectPtr;
    typedef std::list<GameObjectObjectPtr> ObjectList;
    
    //
    //--------------------------------------------------------------------------
    //
    
    class FieldInfo: public GameStateSerializable {
    public:
        void load(const rapidjson::Value& state) override;
        
        const GameObject* getObject(const cocos2d::Vec2& position) const;
        GameObject* getObject(const cocos2d::Vec2& position);
        
        void addObject(const GameObjectObjectPtr& ptr);
        void removeObject(const cocos2d::Vec2& position);
        
    protected:
        ObjectList _objects;
    };
    
    typedef cocos2d::RefPtr<FieldInfo> FieldInfoPtr;

    //
    //--------------------------------------------------------------------------
    //
    
    class EnemyWave: public GameStateSerializable {
    public:
        void load(const rapidjson::Value& state) override;
        
        std::string nextEnemy() const;
        void removeAddedEnemy();
        
        CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _respawnTime, RespawnTime);
        CC_SYNTHESIZE_READONLY_PASS_BY_REF(std::list<std::string>, _enemyList, EnemyList);
        CC_SYNTHESIZE_PASS_BY_REF(time_t, _lastSpawn, LastSpawnTime);
        
    };
	
	typedef cocos2d::RefPtr<EnemyWave> EnemyWavePtr;
	
	//
	//--------------------------------------------------------------------------
	//
	
	class Shop: public GameStateSerializable {
	public:
		void load(const rapidjson::Value& state) override;
		
		CC_SYNTHESIZE_READONLY_PASS_BY_REF(std::vector<std::string>, _availableItems, AvailableItems);
	};
    
    typedef cocos2d::RefPtr<Shop> ShopPtr;
    
    //
    //--------------------------------------------------------------------------
    //

    class GameState: public cocos2d::Ref {
    
    public:
        void load(const rapidjson::Document& state);
        
        const ActorInfo* actorInfo() const {
            return _actor;
        }
        
        //
        
        ActorInfo* actorInfo() {
            return _actor;
        }
        
        //
        
        const FieldInfo* fieldInfo() const {
            return _field;
        }
        
        //
        
        FieldInfo* fieldInfo() {
            return _field;
        }
        
        //
        
        const EnemyWave* waveInfo() const {
            return _wave;
        }
        
        //
        
        EnemyWave* waveInfo() {
            return _wave;
        }
		
		//
		
		const Shop* shopInfo() const {
			return _shop;
		}
		
		//
		
		Shop* shopInfo() {
			return _shop;
		}
		
    protected:
        void loadField(const rapidjson::Document& readfrom);
        void loadActor(const rapidjson::Document& readfrom);
        void loadWave(const rapidjson::Document& readfrom);
		void loadShop(const rapidjson::Document& readfrom);
        
    protected:
        ActorInfoPtr _actor;
        FieldInfoPtr _field;
        EnemyWavePtr _wave;
		ShopPtr _shop;
    };
    
    typedef cocos2d::RefPtr<GameState> GameStatePtr;
}

#endif /* GameState_hpp */
