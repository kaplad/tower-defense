//
//  GameState.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "GameState.hpp"

namespace gamestate {

    void ActorInfo::load(const rapidjson::Value& state) {
        _coins = state["coinscount"].GetUint();
        _lives = state["livescount"].GetUint();
    }
    
    //
    //--------------------------------------------------------------------------
    //
    
    void GameObject::load(const rapidjson::Value& state) {
        const auto& position = state["position"];
        assert(position.IsObject());
        
        _position.x = position["x"].GetInt();
        _position.y = position["y"].GetInt();
        
        _health = 0;
        _type = state["type"].GetString();
    }
    
    //
    //--------------------------------------------------------------------------
    //

    void FieldInfo::load(const rapidjson::Value& state) {
        const auto& objects = state["objects"];
        assert(objects.IsArray());
        
        for(rapidjson::SizeType i = 0; i < objects.Size(); ++i) {
            auto newObject = new GameObject();
            newObject->load(objects[i]);
            addObject(newObject);
        }
    }
    
    //
    //---
    //
    
    void FieldInfo::addObject(const GameObjectObjectPtr& newObject) {
        _objects.push_back(newObject);
    }
    
    //
    //---
    //
    
    void FieldInfo::removeObject(const cocos2d::Vec2& position) {
        _objects.erase(std::find_if(_objects.begin(),
                                    _objects.end(),
                                    [position](const ObjectList::value_type& object){
                                        return object->getPosition() == position;
                                    }));
    }
    
    //
    //---
    //
    
    const GameObject* FieldInfo::getObject(const cocos2d::Vec2& position) const {
        const auto foundposition = std::find_if(_objects.begin(),
                                                _objects.end(),
                                                [&position](const ObjectList::value_type& value) {
                                                    return value->getPosition() == position;
                                                });
        if (foundposition != _objects.end())
            return *foundposition;
        else
            return nullptr;
    }
    
    //
    //---
    //
    
    GameObject* FieldInfo::getObject(const cocos2d::Vec2& position) {
        const auto foundposition = std::find_if(_objects.begin(),
                                                _objects.end(),
                                                [&position](const ObjectList::value_type& value) {
                                                    return value->getPosition() == position;
                                                });
        if (foundposition != _objects.end())
            return *foundposition;
        else
            return nullptr;
    }
    
    //
    //--------------------------------------------------------------------------
    //
    
    void EnemyWave::load(const rapidjson::Value& state) {
        _respawnTime = state["respawmtime"].GetUint();
        
        const auto& objects = state["objects"];
        assert(objects.IsArray());
        
        for(rapidjson::SizeType i = 0; i < objects.Size(); ++i)
            _enemyList.push_back(objects[i].GetString());
        
        _lastSpawn = 0;
    }
    
    //
    //--------------------------------------------------------------------------
    //
    
    std::string EnemyWave::nextEnemy() const {
        if (not _enemyList.empty())
            return _enemyList.front();
        else
            return "";
    }
    
    //
    //---
    //
    
    void EnemyWave::removeAddedEnemy() {
        if (not _enemyList.empty())
            _enemyList.pop_front();
    }
	
	//
	//--------------------------------------------------------------------------
	//
	
	void Shop::load(const rapidjson::Value& state) {
		const auto& items = state["availableitems"];
		assert(items.IsArray());
		
		for(rapidjson::SizeType i = 0; i < items.Size(); ++i)
			_availableItems.push_back(items[i].GetString());
	}
	
    //
    //--------------------------------------------------------------------------
    //
    
    void GameState::load(const rapidjson::Document& state) {
        loadActor(state);
        loadField(state);
        loadWave(state);
		loadShop(state);
    }
    
    //
    //---
    //
    
    void GameState::loadField(const rapidjson::Document& readfrom) {
        const auto& field = readfrom["field"];
        assert(field.IsObject());

        _field = new FieldInfo();
        _field->load(field);
    }
    
    //
    //---
    //
    
    void GameState::loadActor(const rapidjson::Document& readfrom) {
        const auto& actor = readfrom["actorinfo"];
        assert(actor.IsObject());
        
        _actor = new ActorInfo();
        _actor->load(actor);
    }
    
    //
    //---
    //
    
    void GameState::loadWave(const rapidjson::Document& readfrom) {
        const auto& wave = readfrom["enemywave"];
        assert(wave.IsObject());
        
        _wave = new EnemyWave();
        _wave->load(wave);
    }
	
	//
	//---
	//
	
	void GameState::loadShop(const rapidjson::Document& readfrom) {
		const auto& shop = readfrom["shop"];
		assert(shop.IsObject());
		
		_shop = new Shop();
		_shop->load(shop);
	}
	
}
