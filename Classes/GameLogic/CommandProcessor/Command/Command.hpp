//
//  Command.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef Command_hpp
#define Command_hpp

#include "ObjectInfoProvider.hpp"

namespace gamestate {
    
    class GameState;
	class GameObject;
	
    typedef std::string CommandResult;
    
    //
    //--------------------------------------------------------------------------
    //
    
    class Command: public cocos2d::Ref {
        
    public:
        virtual void execute(GameState* const state) const = 0;
        
        virtual ~Command() {}
        CC_SYNTHESIZE_PASS_BY_REF(std::function<void()>, _success, SuccessCallback)
        CC_SYNTHESIZE_PASS_BY_REF(std::function<void(const CommandResult&)>, _error, ErrorCallback)
		
	protected:
		template <class T>
		const T* objectInfoFor(const std::string& type) const {
			auto objectinfo = dynamic_cast<const T*>(ObjectInfoProvider::instance().objectInfo(type));
			if (!objectinfo)
				throw std::runtime_error("No information for object: " + type);
			
			return objectinfo;
		}
		
		GameObject* findObjectOnField(GameState* const state,
									  const cocos2d::Vec2 position) const;
    };

    typedef cocos2d::RefPtr<Command> CommandPtr;
    
    //
    //--------------------------------------------------------------------------
    //
    
    class AddTowerCommand: public Command {
    public:
        AddTowerCommand(const cocos2d::Vec2& position,
                        const std::string& type);
                        
        virtual void execute(GameState* const state) const override;
        
    protected:
        const cocos2d::Vec2 _position;
        const std::string _type;
    };

    //
    //--------------------------------------------------------------------------
    //
    
    class AddEnemyCommand: public Command {
    public:
        AddEnemyCommand(const cocos2d::Vec2& position,
                        const std::string& type);
        
        virtual void execute(GameState* const state) const override;
        
    protected:
        const cocos2d::Vec2 _position;
        const std::string _type;
    };
    
    //
    //--------------------------------------------------------------------------
    //
    
    class MoveEnemyCommand: public Command {
    public:
        MoveEnemyCommand(const cocos2d::Vec2& position_old,
                        const cocos2d::Vec2& position_new);
        
        virtual void execute(GameState* const state) const override;
        
    protected:
        const cocos2d::Vec2 _position_old;
        const cocos2d::Vec2 _position_new;
    };
    
    //
    //--------------------------------------------------------------------------
    //
    
    class TowerAttack: public Command {
    public:
        TowerAttack(const cocos2d::Vec2& towerPosition,
                    const cocos2d::Vec2& enemyPosition);
        
        virtual void execute(GameState* const state) const override;
        
    protected:
        const cocos2d::Vec2 _towerPosition;
        const cocos2d::Vec2 _enemyPosition;
    };
}

#endif /* Command_hpp */
