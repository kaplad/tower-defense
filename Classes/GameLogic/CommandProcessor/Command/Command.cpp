//
//  Command.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "Command.hpp"
#include "GameState.hpp"
#include "WidgetConst.h"
#include "GameClock.h"

namespace gamestate {

	GameObject* Command::findObjectOnField(GameState* const state,
										   const cocos2d::Vec2 position) const {
		const auto foundobject = state->fieldInfo()->getObject(position);
		if (not foundobject)
			throw std::runtime_error("No object on field");
		
		return foundobject;
	}
	
	//
	//--------------------------------------------------------------------------
	//
	
    AddTowerCommand::AddTowerCommand(const cocos2d::Vec2& position,
                                     const std::string& type):
    _type(type),
    _position(position)
    {
    
    }

    //
    //---
    //
    
    void AddTowerCommand::execute(GameState* const state) const {
		const auto foundobject = state->fieldInfo()->getObject(_position);
        if (foundobject)
			throw std::runtime_error("Object already exists");
        
		const auto objectinfo = objectInfoFor<TowerObjectInfoBase>(_type);
        
        const uint userhas = state->actorInfo()->getCoins();
        const uint objectcosts = objectinfo->getPrice();
        
        if (userhas < objectcosts)
			throw std::runtime_error("Not enought money for object: " + _type);
        
        //
        
        state->actorInfo()->setCoins(userhas - objectcosts);
        
        const auto newObject = new gamestate::GameObject();
        newObject->setNextUpdateTime(GameClock::instance().nowtime() + objectinfo->getUpdateTime());
        newObject->setPosition(_position);
        newObject->setType(_type);
        
        state->fieldInfo()->addObject(newObject);
        
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(COINS_CHANGE_EVENT);
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FIELD_CHANGE_EVENT);
    }
    
    //
    //--------------------------------------------------------------------------
    //
    
    AddEnemyCommand::AddEnemyCommand(const cocos2d::Vec2& position,
                                     const std::string& type):
    _type(type),
    _position(position)
    {
        
    }
    
    //
    //---
    //
    
    void AddEnemyCommand::execute(GameState* const state) const {
		const auto foundobject = state->fieldInfo()->getObject(_position);
        if (foundobject)
			throw std::runtime_error("Object already exists");

		const auto objectinfo = objectInfoFor<EnemyObjectInfoBase>(_type);

        const auto now = GameClock::instance().nowtime();
        const time_t lastspawn = state->waveInfo()->getLastSpawnTime();
        const uint spawnPause = state->waveInfo()->getRespawnTime();
        
        if (now < lastspawn + spawnPause)
			throw std::runtime_error("Too early to spawn enemy");
        
        const auto newObject = new gamestate::GameObject();
        newObject->setNextUpdateTime(now + spawnPause);
        newObject->setPosition(_position);
        newObject->setType(_type);
        newObject->setHealth(objectinfo->getHealth());
        
        state->fieldInfo()->addObject(newObject);
        state->waveInfo()->removeAddedEnemy();
        state->waveInfo()->setLastSpawnTime(now);
        
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FIELD_CHANGE_EVENT);
    }
    
    //
    //--------------------------------------------------------------------------
    //
    
    MoveEnemyCommand::MoveEnemyCommand(const cocos2d::Vec2& position_old,
                                       const cocos2d::Vec2& position_new):
    _position_old(position_old),
    _position_new(position_new)
    {
    }
    
    //
    //---
    //
    
    void MoveEnemyCommand::execute(GameState* const state) const {
        const auto foundobject = findObjectOnField(state, _position_old);
		const auto foundobject_newcoord = state->fieldInfo()->getObject(_position_new);
        if (foundobject_newcoord)
			throw std::runtime_error("Can not move enemy to already occupied cell");
		
		const auto objectinfo = objectInfoFor<EnemyObjectInfoBase>(foundobject->getType());
        
        foundobject->setPosition(_position_new);
        foundobject->setNextUpdateTime(GameClock::instance().nowtime() + objectinfo->getUpdateTime());
        
        cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FIELD_CHANGE_EVENT);
    }
    
    //
    //--------------------------------------------------------------------------
    //
    
    TowerAttack::TowerAttack(const cocos2d::Vec2& towerPosition,
                             const cocos2d::Vec2& enemyPosition):
    _towerPosition(towerPosition),
    _enemyPosition(enemyPosition)
    {
    }
    
    //
    //---
    //
    
    void TowerAttack::execute(GameState* const state) const {
        const auto tower = findObjectOnField(state, _towerPosition);
        const auto enemy = findObjectOnField(state, _enemyPosition);
		
		const auto towerinfo = objectInfoFor<TowerObjectInfoBase>(tower->getType());
		const auto enemyinfo = objectInfoFor<EnemyObjectInfoBase>(enemy->getType());
        
        const auto towerpower = towerinfo->getHitPower();
        const auto enemyhealth = enemy->getHealth();
        
        if (towerpower >= enemyhealth) {
            state->fieldInfo()->removeObject(_enemyPosition);
			state->actorInfo()->setCoins(state->actorInfo()->getCoins() + enemyinfo->getKillReward());
			
            cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FIELD_CHANGE_EVENT);
			cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(COINS_CHANGE_EVENT);
        }
        else
            enemy->setHealth(enemyhealth - towerpower);
        
        tower->setNextUpdateTime(GameClock::instance().nowtime() + towerinfo->getUpdateTime());
    }
}
