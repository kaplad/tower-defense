//
//  CommandProcessor.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#ifndef CommandProcessor_hpp
#define CommandProcessor_hpp

#include "GameState.hpp"
#include "Command.hpp"

namespace gamestate {
    
    class CommandProcessor: public cocos2d::Ref {
        
    public:
        bool init(const std::string& statepath);
        
        const GameState* getState() const;
        
        void executeCommand(CommandPtr command);
        
        void update();
        
    protected:
        GameStatePtr _state;
        std::list<CommandPtr> _waitingCommands;
    };
    
    typedef cocos2d::RefPtr<CommandProcessor> CommandProcessorPtr;
}

#endif /* CommandProcessor_hpp */
