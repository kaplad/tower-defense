//
//  CommandProcessor.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/31/16.
//
//

#include "CommandProcessor.hpp"
#include "Command.hpp"

namespace gamestate {
    
    bool CommandProcessor::init(const std::string &statepath) {
        rapidjson::Document doc;
        std::string contentStr = cocos2d::FileUtils::getInstance()->getStringFromFile(statepath);
        doc.Parse<0>(contentStr.c_str());
        
        _state = new GameState();
        _state->load(doc);
        
        
        return true;
    }
    
    //
    //---
    //
    
    const GameState* CommandProcessor::getState() const {
        return _state;
    }
    
    //
    //---
    //
    
    void CommandProcessor::executeCommand(cocos2d::RefPtr<Command> command) {
        _waitingCommands.push_back(command);   
    }
    
    //
    //---
    //
    
    void CommandProcessor::update() {
        for (const auto& command : _waitingCommands) {
			try {
				command->execute(_state);
			}
			catch (const std::runtime_error& ex) {
				const auto callback = command->getErrorCallback();
				if (callback) callback(ex.what());
				
				continue;
			}
			
			const auto callback = command->getSuccessCallback();
			if (callback) callback();
        }
        
        _waitingCommands.clear();
    }
}
