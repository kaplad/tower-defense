//
//  ObjectInfo.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef ObjectInfo_hpp
#define ObjectInfo_hpp

#include "json/document.h"

class GameObjectWidget;

class ObjectInfo: public cocos2d::Ref {
    make_baseclass(cocos2d::Node)
    
public:
    virtual void load(const rapidjson::Value& node);
    
    virtual GameObjectWidget* prototype() const = 0;
    
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(std::string, _name, Name)
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(std::string, _image, Image)
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _updateTime, UpdateTime)
};

//
//---
//

class TowerObjectInfoBase: public ObjectInfo {
    make_baseclass(ObjectInfo)
    
public:
    virtual void load(const rapidjson::Value& node) override;
    
    virtual GameObjectWidget* prototype() const override;
    
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _price, Price)
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _hitRadius, HitRadius)
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _hitPower, HitPower)
};

//
//---
//

class EnemyObjectInfoBase: public ObjectInfo {
    make_baseclass(ObjectInfo)
    
public:
    virtual void load(const rapidjson::Value& node) override;
    
    virtual GameObjectWidget* prototype() const override;
    CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _health, Health)
	CC_SYNTHESIZE_READONLY_PASS_BY_REF(uint, _killReward, KillReward)
    
};

#endif /* ObjectInfo_hpp */
