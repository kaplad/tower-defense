//
//  ObjectInfo.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#include "ObjectInfo.hpp"
#include "GameObjectWidget.hpp"

void ObjectInfo::load(const rapidjson::Value& node) {
    _name = node["name"].GetString();
    _image = node["image"].GetString();
    _updateTime = node["updatetime"].GetUint();
}

//
//------------------------------------------------------------------------------
//

GameObjectWidget* TowerObjectInfoBase::prototype() const {
    return TowerObjectWidget::create(this);
}

//
//---
//

void TowerObjectInfoBase::load(const rapidjson::Value& node) {
    baseclass::load(node);
    
    _price = node["price"].GetUint();
    _hitRadius = node["hitradius"].GetUint();
    _hitPower = node["hitpower"].GetUint();
}

//
//------------------------------------------------------------------------------
//

GameObjectWidget* EnemyObjectInfoBase::prototype() const {
    return EnemyObjectWidget::create(this);
}

//
//---
//

void EnemyObjectInfoBase::load(const rapidjson::Value& node) {
    baseclass::load(node);
    
    _health = node["health"].GetUint();
	_killReward = node["killreward"].GetUint();
}
