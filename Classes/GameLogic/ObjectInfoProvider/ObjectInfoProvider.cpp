//
//  ObjectInfoProvider.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#include "ObjectInfoProvider.hpp"


void ObjectInfoProvider::load(const rapidjson::Document& document) {
    assert(document.IsArray());
    
    for(rapidjson::SizeType i = 0; i < document.Size(); ++i) {
        assert(document[i].IsObject());
        
        const auto& value = document[i];
        const auto superclass = value["superclass"].GetString();
        
        auto info = create(superclass);
        info->load(value);
        
        cocos2d::RefPtr<ObjectInfo> some(new TowerObjectInfoBase());
        
        _classInstances.insert(std::make_pair(info->getName(),
                                              info));
    }
}

//
//---
//

template <class T>
T* createInstance() {
    return new T();
}

void ObjectInfoProvider::enumerateCLasses() {
    _classes["tower"] = createInstance<TowerObjectInfoBase>;
    _classes["enemy"] = createInstance<EnemyObjectInfoBase>;
}
