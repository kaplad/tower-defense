//
//  ObjectInfoProvider.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/30/16.
//
//

#ifndef ObjectInfoProvider_hpp
#define ObjectInfoProvider_hpp

#include "Factory.h"
#include "ObjectInfo.hpp"
#include "Singleton.h"

class ObjectInfoProvider: public ClassFactory<std::string, ObjectInfo>,
                          public LazyNonThreadSafeSingleton<ObjectInfoProvider> {
    
protected:
    virtual void load(const rapidjson::Document& document);
    virtual void enumerateCLasses();
};

#endif /* ObjectInfoProvider_hpp */
