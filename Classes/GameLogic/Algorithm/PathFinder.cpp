//
//  PathFinder.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 11/2/16.
//
//

#include "PathFinder.hpp"
#include "TileValueStrategy.hpp"
#include "AStar.h"

PathFinder::Map PathFinder::generateMapWithStrategy(const TileValueStrategy& strategy,
													const cocos2d::Size& mapsize) const {
	Map map (mapsize.width, MapRow(mapsize.height));
	
	for(size_t i = 0; i < mapsize.width; ++i) {
		for (size_t j = 0; j < mapsize.height; ++j) {
			map[i][j] = !strategy.tileValue(cocos2d::Vec2(i, j));
		}
	}
	
	return map;
}

//
//---
//

PathFinder::PathList PathFinder::findPath(const Map& maptosearch,
										  const cocos2d::Vec2& from,
										  const cocos2d::Vec2& to) const {
	const std::string result = pathFind(maptosearch,
										Location(from.x, from.y),
										Location(to.x, to.y),
										16,
										16); // todo
	
	
	return convert(result, from);
}

//
//---
//

PathFinder::PathList PathFinder::convert(const std::string& path,
										 const cocos2d::Vec2& from) const {
	PathList result;
	cocos2d::Vec2 current = from;
	
	for(int i = 0; i < path.length(); i++) {
		char c = path.at(i);
		int number = atoi(&c);
		int x = iDir[number];
		int y = jDir[number];
		
		current += cocos2d::Vec2(x, y);
		
		result.push_back(current);
	}
	
	return result;
	
}
