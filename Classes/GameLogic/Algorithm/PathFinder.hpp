//
//  PathFinder.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 11/2/16.
//
//

#ifndef PathFinder_hpp
#define PathFinder_hpp

class TileValueStrategy;

class PathFinder: cocos2d::Ref {

public:
	typedef std::list<cocos2d::Vec2> PathList;
	typedef std::vector<int> MapRow;
	typedef std::vector<MapRow> Map;
	
	PathList findPath(const Map& maptosearch,
					  const cocos2d::Vec2& from,
					  const cocos2d::Vec2& to) const;
	
	Map generateMapWithStrategy(const TileValueStrategy&,
								const cocos2d::Size& mapsize) const;
	
	PathList convert(const std::string& path,
					 const cocos2d::Vec2& from) const;
	
	
	
};

#endif /* PathFinder_hpp */
