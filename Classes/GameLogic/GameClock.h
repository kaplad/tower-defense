//
//  GameClock.h
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 11/1/16.
//
//

#ifndef GameClock_h
#define GameClock_h

#include "Singleton.h"

class GameClock: public LazyNonThreadSafeSingleton<GameClock> {
public:
    time_t nowtime() const { // needs to be real game clock
        return std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    }
};

#endif /* GameClock_h */
