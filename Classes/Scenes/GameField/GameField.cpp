//
//  GameField.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#include "GameField.hpp"
#include "ShopWidget.hpp"
#include "ShopItemWidget.hpp"
#include "GameFieldWidget.hpp"
#include "WidgetConst.h"
#include "GameState.hpp"

GameField::GameField(const std::string& stateToLoad): _stateToLoad(stateToLoad) {
	
}

//
//---
//

cocos2d::Scene* GameField::createScene(const std::string& state) {
    auto scene = cocos2d::Scene::create();
    auto layer = GameField::create(state);
    
    scene->addChild(layer);
    
    return scene;
}

//
//---
//

bool GameField::init() {
    if(!baseclass::init())
        return false;
    
    if (!loadState())
        return false;
    
    if (!createMap())
        return false;
    
    if (!createShop() or !createControls())
        return false;
    
    if (!createGame())
        return false;
    
    scheduleUpdate();
    
    return true;
}

//
//---
//

bool GameField::createMap() {
    const auto map = GameFieldWidget::create("tilemap.tmx");
    if (!map)
        return false;
    
    _fieldmap = map;
    addChild(_fieldmap);
    
    return true;
}

//
//---
//

bool GameField::createShop() {
    const auto shop = ShopWidget::create(_commandprocessor,
                                         _fieldmap);
    if (!shop)
        return false;
    
    _shopWidget = shop;
	
	for( const auto& item : _commandprocessor->getState()->shopInfo()->getAvailableItems())
		_shopWidget->addChild(ShopItemWidget::create(item));
	
    _shopWidget->setAnchorPoint(cocos2d::Vec2());
    _shopWidget->setPosition(cocos2d::Vec2(768, 0));
    addChild(_shopWidget);
    
    return true;
}

//
//---
//

bool GameField::loadState() {
    _commandprocessor = new gamestate::CommandProcessor();
    
    return _commandprocessor->init(_stateToLoad);
        return false;
}

//
//---
//

void GameField::update(float delta) {
    if (_commandprocessor)
        _commandprocessor->update();
    
    if (_controller)
        _controller->update();
}

//
//---
//

bool GameField::createGame() {
    _controller = new GameController(_commandprocessor, _fieldmap);
    
    return true;
}

//
//---
//

bool GameField::createControls() {
    auto coins_label = cocos2d::Label::createWithTTF("coins: " + std::to_string(_commandprocessor->getState()->actorInfo()->getCoins()),
                                           "fonts/Marker Felt.ttf", 26);
    coins_label->setPosition(785, 730);
    coins_label->setAnchorPoint(cocos2d::Vec2::ANCHOR_BOTTOM_LEFT);
    addChild(coins_label);

    getEventDispatcher()->addCustomEventListener(COINS_CHANGE_EVENT, [this, coins_label](cocos2d::EventCustom* event){
        coins_label->setString("coins: " + std::to_string(_commandprocessor->getState()->actorInfo()->getCoins()));
    });
    
    auto live_label = cocos2d::Label::createWithTTF("lives: " + std::to_string(_commandprocessor->getState()->actorInfo()->getLives()),
                                           "fonts/Marker Felt.ttf", 26);
    live_label->setAnchorPoint(cocos2d::Vec2::ANCHOR_BOTTOM_LEFT);
    live_label->setPosition(785, 670);
    addChild(live_label);
    
    getEventDispatcher()->addCustomEventListener(LIVES_CHANGE_EVENT, [this, live_label](cocos2d::EventCustom* event){
        live_label->setString("lives: " + std::to_string(_commandprocessor->getState()->actorInfo()->getLives()));
    });
    
    return true;
}

//
//---
//

void GameField::onExit() {
	baseclass::onExit();
	
	getEventDispatcher()->removeCustomEventListeners(COINS_CHANGE_EVENT);
	getEventDispatcher()->removeCustomEventListeners(LIVES_CHANGE_EVENT);
}
