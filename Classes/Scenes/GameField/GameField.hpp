//
//  GameField.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 10/29/16.
//
//

#ifndef GameField_hpp
#define GameField_hpp

#include "GameController.hpp"
#include "WidgetConst.h"

class GameFieldWidget;
class ShopWidget;

class GameField: public cocos2d::Layer {
    make_baseclass(cocos2d::Layer);
    
public:
	static cocos2d::Scene* createScene(const std::string& state);
	make_create(GameField);
	
	GameField(const std::string& stateToLoad);
	
	void onExit() override;
	
protected:
    virtual bool init() override;

    bool createMap();
    bool createControls();
    bool createShop();
    bool loadState();
    bool createGame();
    
    void update(float delta) override;

protected:
    GameFieldWidget* _fieldmap;
    ShopWidget* _shopWidget;
    
    gamestate::CommandProcessorPtr _commandprocessor;
    GameControllerPtr _controller;
	
	std::string _stateToLoad;
	
};

#endif /* GameField_hpp */
