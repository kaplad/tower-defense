//
//  MainMenu.cpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 11/6/16.
//
//

#include "MainMenu.hpp"
#include "GameField.hpp"

cocos2d::Scene* MainMenu::createScene() {
	auto scene = cocos2d::Scene::create();
	auto layer = MainMenu::create();
	
	scene->addChild(layer);
	
	return scene;
}

//
//---
//

bool MainMenu::init() {
	if (not baseclass::init())
		return false;
	
	auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
	auto level1 = cocos2d::MenuItemLabel::create(cocos2d::Label::createWithTTF("Level 1",
																				  "fonts/Marker Felt.ttf",
																				  26),
													[](Ref*){
														cocos2d::Director::getInstance()->replaceScene(GameField::createScene("state.json"));
													});
	level1->setPosition(0, 30);
	
	auto level2 = cocos2d::MenuItemLabel::create(cocos2d::Label::createWithTTF("Level 2",
																				  "fonts/Marker Felt.ttf",
																				  26),
													[](Ref*){
														cocos2d::Director::getInstance()->replaceScene(GameField::createScene("state_level2.json"));
													});
	

	auto menu = cocos2d::Menu::create(level1, level2, NULL);
	menu->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	menu->setAnchorPoint(cocos2d::Vec2::ANCHOR_MIDDLE);
	addChild(menu);
	
	return true;
}
