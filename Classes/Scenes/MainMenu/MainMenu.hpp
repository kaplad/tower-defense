//
//  MainMenu.hpp
//  TowerDefense
//
//  Created by Vladyslav Kaplun on 11/6/16.
//
//

#ifndef MainMenu_hpp
#define MainMenu_hpp

class MainMenu: public cocos2d::Layer {
	make_baseclass(cocos2d::Layer);
	
public:
	static cocos2d::Scene* createScene();
	CREATE_FUNC(MainMenu);
	
protected:
	virtual bool init() override;
};

#endif /* MainMenu_hpp */
